FROM python:3-alpine

ENV user admin
ENV password pass
ENV email admin@admin.ru

WORKDIR /app

RUN mkdir /app/db
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .

EXPOSE 8000
VOLUME ["/app/db"]

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000